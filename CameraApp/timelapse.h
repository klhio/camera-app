/*
 * Copyright (C) 2021 UBports Foundation.
 * Author(s): Lorenzo Torracchi <lorenzotorracchi@mail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TIMELAPSE_H
#define TIMELAPSE_H

#include <QObject>

class TimeLapse : public QObject
{
Q_OBJECT

public:
    explicit TimeLapse(QObject *parent = 0);

    Q_INVOKABLE void makeTimeLapseVideo(QString sourceDir, QString targetDir);
};

#endif //TIMELAPSE_H

/*
 * Copyright (C) 2021 UBports Foundation.
 * Author(s): Lorenzo Torracchi <lorenzotorracchi@mail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timelapse.h"
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core/utils/logger.hpp>

#include <QDebug>
#include <QString>
#include <QDateTime>
#include <QDir>

using namespace cv;

TimeLapse::TimeLapse(QObject *parent) : QObject(parent) {
}

void TimeLapse::makeTimeLapseVideo(QString sourceDir, QString targetDir) {
    utils::logging::setLogLevel(utils::logging::LOG_LEVEL_VERBOSE);
    QDateTime currentDateTime = QDateTime::currentDateTime();
    QString fileName = targetDir + "/" + currentDateTime.toString("yyyy-MM-dd-hh_mm_ss") + ".avi";
    qDebug() << "************** outfile: " << fileName;

    QDir dir(sourceDir);
    QFileInfoList frames = dir.entryInfoList(QStringList() << "*.jpg" << "*.jpeg" << "*.JPG" << "*.JPEG", QDir::Files);
    if (frames.isEmpty()) {
        qDebug() << "************** No Frames available";
        return;
    }

    qDebug() << "************** Started writing video... ";
    Mat image = imread(frames.at(0).absoluteFilePath().toStdString().c_str(), IMREAD_GRAYSCALE);
    if (image.empty()) {
        qDebug() << "cannot read image" << frames.at(0).absoluteFilePath();
        dir.removeRecursively();
        return;
    }
    VideoWriter writer;
    int codec = VideoWriter::fourcc('X', 'V', 'I', 'D');
    double fps = 5.0;
    writer.open(fileName.toStdString(), codec, fps, image.size(), true);
    if (!writer.isOpened()) {
        qDebug() << "Could not open the output video file for write";
        dir.removeRecursively();
        return;
    }
    qDebug() << "************** files: " << frames.size();
    for (int i = 0; i < frames.size(); i++) {
        image = imread(frames.at(i).absoluteFilePath().toStdString().c_str(), IMREAD_GRAYSCALE);
        if (image.empty()) {
            qDebug() << "************** skipped empty image: " << frames.at(i).absoluteFilePath();
            continue;
        }
        writer.write(image);
    }
    qDebug() << "************** Write completed!";

    dir.removeRecursively();
    qDebug() << "************** FINITO!";
}
